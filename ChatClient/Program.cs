﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace ChatClient
{
    class Program
    {
        static void Main(string[] args)
        {
            InstanceContext context = new InstanceContext(new Chat());
            DuplexChannelFactory<IChat> factory;
            IChat chat;
            IClientChannel client;

            // Binding
            var binding = new NetTcpBinding();

            Creating:

            // Создание фабрики дуплексных каналов
            factory = new DuplexChannelFactory<IChat>(
               context,
               binding,
               "net.tcp://localhost:8002/ChatService");

            chat = factory.CreateChannel();
            client = (IClientChannel)chat;

            Console.WriteLine("Введите имя");
            string name = Console.ReadLine();
            Console.WriteLine(chat.EnterChat() + "\n________________________");

            string message;

            while (true)
            {
                try
                {
                    if (factory != null)
                    {
                        message = Console.ReadLine();

                        if (client.State == (CommunicationState.Faulted | CommunicationState.Closed))
                        {
                            throw new CommunicationException();
                        }
                        chat.SendMessage(name, message);
                    }
                    else goto Creating;
                }
                catch (CommunicationException)
                {
                    Console.WriteLine("Потеря связи, войдите заново");
                    factory.Abort();
                    factory = null;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    factory.Abort();
                    factory = null;
                }

            }
        }
    }
}
