﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient
{
    class Chat : Contracts.IChatCallback
    {
        public void NoticeMessage(string name, string message)
        {
            Console.WriteLine("\n===============\n{0}: {1} \n===============\n\n", name, message);
        }
    }
}
