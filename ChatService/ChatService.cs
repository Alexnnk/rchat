﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Threading;

namespace ChatService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    class ChatService : IChat
    {
        // Подключенные пользователи
        Dictionary<Guid, IChatCallback> users = new Dictionary<Guid, IChatCallback>();
        object syncObj = new object();

        IChatCallback Callback
        {
            get { return OperationContext.Current.GetCallbackChannel<IChatCallback>(); }
        }

        public string EnterChat()
        {
            lock (syncObj)
            {
                Guid uid = Guid.NewGuid();
                OperationContext.Current.Channel.Closed += new EventHandler(Faulted);

                foreach (var u in users)
                {
                    // if ((u.Value as ICommunicationObject).State == CommunicationState.Opened)
                    u.Value.NoticeMessage(u.Key.ToString(), " - Подключился");
                }

                users.Add(uid, Callback);
                Console.WriteLine("Подключен пользователь " + uid);

                return "Вы вошли в чат. Пишите сообщение:";
            }
        }
        public void SendMessage(string name, string message)
        {
            lock (syncObj)
            {
                foreach (var user in users)
                {
                    Console.WriteLine();
                    user.Value.NoticeMessage(name, message);
                }
            }
        }
        /// <summary>
        /// Удаление пользователя из "подключенных" при закрытии клиента
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Faulted(object sender, EventArgs e)
        {
            var callback = (IChatCallback)sender;
            KeyValuePair<Guid, IChatCallback> user = users.First(x => x.Value == callback);
            lock (syncObj)
            {
                this.users.Remove(user.Key);
            }
            Console.WriteLine("Отсоединен " + user.Key);
        }
    }
}
