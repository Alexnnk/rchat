﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace Contracts
{
    [ServiceContract(CallbackContract = typeof(IChatCallback),
        Name = "Chat",
        SessionMode = SessionMode.Required)] 

    public interface IChat
    {         
        /// <summary>
        /// Операция подключения к чату
        /// </summary>
        /// <returns>Информация о результате подключения</returns>
        [OperationContract]
        string EnterChat();

        /// <summary>
        /// Операция отправки сообщения
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="message">Сообщение</param>
        [OperationContract(IsOneWay = true)]
        void SendMessage(string name, string message);
    }

    public interface IChatCallback
    {
        /// <summary>
        /// Вывод сообщений от других пользователей
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="message">Сообщение</param>
        [OperationContract(IsOneWay = true)]
        void NoticeMessage(string name, string message);
    }
}
